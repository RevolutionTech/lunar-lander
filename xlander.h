/* - - - - - - - - - - - - - - - - - - - - - -
Name: Lucas Connors
Course: CS 349
Assignment: A1
File: xlander.h
Note: Some skeleton code taken from XWindows Examples from the course web site
- - - - - - - - - - - - - - - - - - - - - - */

#ifndef __XLANDER_H__
#define __XLANDER_H__

/* General header files */
#include <iostream>
#include <algorithm>	//for min and max
#include <list>
#include <vector>
#include <cstdlib>
#include <sys/time.h>

/* Header files for X functions */
#include <X11/Xlib.h>
#include <X11/Xutil.h>

using namespace std;

const int FPS = 30;

class Displayable;

/* Header files for Xlander */
#include "paint.h"
#include "window.h"
#include "ship.h"
#include "ground.h"
#include "collision.h"

#endif /* __XLANDER_H__ */
