/* - - - - - - - - - - - - - - - - - - - - - -
Name: Lucas Connors
Course: CS 349
Assignment: A1
File: xlander.cpp
Note: Some skeleton code taken from XWindows Examples from the course web site
- - - - - - - - - - - - - - - - - - - - - - */

#include <unistd.h>

#include "xlander.h"

void eventloop(XInfo &xInfo, Controller &controller){
    XEvent event;
    unsigned long lastRepaint = 0;
	double vx;
	double vy;
	
	while (true){
		double speed = controller.player->speed;
		
	    if (XPending(xInfo.display) > 0) {
			XNextEvent(xInfo.display, &event);
		
			switch (event.type){
				case KeyPress:
					switch (XLookupKeysym(&event.xkey, 0)){
						case XK_Left:
							vx = controller.player->vx;
							vx = vx-speed;
							controller.player->vx = vx;
							break;
						case XK_Right:
							vx = controller.player->vx;
							vx = vx+speed;
							controller.player->vx = vx;
							break;
						case XK_Up:
							vy = controller.player->vy;
							vy = vy-(speed*1.5);
							controller.player->vy = vy;
							break;
						case XK_Down:
							vy = controller.player->vy;
							vy = vy+speed;
							controller.player->vy = vy;
							break;
						case XK_space:
							controller.space();
							break;
						case XK_d:
							controller.player->debug_switch();
							break;
						case XK_q:
							XCloseDisplay(xInfo.display);
							exit(0);
							break;
					}
					break;
				case ConfigureNotify:
					handleResize(xInfo, event, controller);
					break;
			}
		}
		
		// timer
		unsigned long end = now();
		if (end - lastRepaint > 1000000/FPS) {
			controller.player->move();
			drawEverything(xInfo, controller);
			collision_check_all(controller);
			lastRepaint = now();
		} else if (XPending(xInfo.display) == 0) {			// give the system time to do other things
			usleep(1000000/FPS - (end - lastRepaint));
		}
	}
}

/*
 * Start executing here.
 *	 First initialize window.
 *	 Next loop responding to events.
 *	 Exit forcing window manager to clean up - cheesy, but easy.
 */
int main(int argc, char *argv[]){
	XInfo xInfo;
	Controller controller;
	initX(argc, argv, xInfo, controller);
	eventloop(xInfo, controller);
	
	return 0;
}
