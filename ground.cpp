/* - - - - - - - - - - - - - - - - - - - - - -
Name: Lucas Connors
Course: CS 349
Assignment: A1
File: ground.cpp
- - - - - - - - - - - - - - - - - - - - - - */

#include "xlander.h"

//constructor
Pad::Pad(int x, int y, int width, int height){
	this->x = x;
	this->y = y;
	this->width = width;
	this->height = height;
	this->in_menu = true;
}
		
Terrain::Terrain() {
	this->in_menu = true;
}

// add another point to the line
void Terrain::add_point(int x, int y){
	XPoint p;
	p.x = x;
	p.y = y;
	pts.push_back(p);
}

// create terrain
void terrain_create(Controller &controller, int i, int x, int y, int xn, int yn){
	controller.terrain[i] = new Terrain();
	while (x < xn){
		controller.terrain[i]->add_point(x,y);
		x += 20 + rand()%40;
		y = 300+(rand()%300);
		if (x >= xn){
			controller.terrain[i]->add_point(xn, yn);
		}
	}
}
