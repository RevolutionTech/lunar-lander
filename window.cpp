/* - - - - - - - - - - - - - - - - - - - - - -
Name: Lucas Connors
Course: CS 349
Assignment: A1
File: window.cpp
- - - - - - - - - - - - - - - - - - - - - - */

#include <unistd.h>

#include "xlander.h"

Warning::Warning(){
	this->display_warning = false;
}

Menu::Menu(){
	this->in_menu = true;
	this->menu_type = MT_MAIN;
}

Controller::Controller(){
	this->in_menu = true;
	this->paused = false;
}
		
void Controller::space(){
	if (in_menu){
		in_menu = false;
		player->unpause();
		player->in_menu = false;
		for (int i=0; i<2; ++i){
			landing_pad[i]->in_menu = false;
		}
		for (int i=0; i<3; ++i){
			terrain[i]->in_menu = false;
		}
		main->in_menu = false;
	}
	else {
		pause();
	}
}
		
void Controller::pause() {
	if (paused){
		paused = false;
		player->unpause();
	}
	else {
		paused = true;
		player->pause();
	}
}

void Controller::destroy_all(){
	in_menu = true;
	main->in_menu = true;
	main->menu_type = MT_GAMEOVER;
	delete player;
	for (int i=0; i<2; ++i){
		delete landing_pad[i];
	}
	for (int i=0; i<3; ++i){
		delete terrain[i];
	}
	initObjects(*this);
}

void Controller::win(){
	in_menu = true;
	main->in_menu = true;
	main->menu_type = MT_WIN;
	delete player;
	for (int i=0; i<2; ++i){
		delete landing_pad[i];
	}
	for (int i=0; i<3; ++i){
		delete terrain[i];
	}
	initObjects(*this);
}

// Function to put out a message on error and exits
void error(string str) {
	cerr << str << endl;
	exit(0);
}

void handleResize(XInfo &xInfo, XEvent &event, Controller &controller) {
	XConfigureEvent xce = event.xconfigure;
	XFreePixmap(xInfo.display, xInfo.pixmap);
	int depth = DefaultDepth(xInfo.display, DefaultScreen(xInfo.display));
	xInfo.pixmap = XCreatePixmap(xInfo.display, xInfo.window, xce.width, xce.height, depth);
	xInfo.width = xce.width;
	xInfo.height = xce.height;
	
	if (xce.width < 800 || xce.height < 600){
		controller.warning->display_warning = true;
	}
	else {
		controller.warning->display_warning = false;
	}
}

void initObjects(Controller &controller){
	// init random seed
	srand(time(NULL));
	
	//create the ship
	int ship_size = 40;
	controller.player = new Ship(controller, 50+ship_size/2+(rand()%(750-ship_size/2)), 25+ship_size/2, ship_size);
	controller.player->pause();
	//create the landing pads
	int pad_width = 100;
	int pad_height = 20;
	int padx[2];
	int pady[2];
	padx[0] = 100+pad_width/2+(rand()%(200-pad_width/2));
	padx[1] = 500+pad_width/2+(rand()%(200-pad_width/2));
	pady[0] = 400+pad_height/2+(rand()%(100-pad_height/2));
	pady[1] = 400+pad_height/2+(rand()%(100-pad_height/2));
	controller.landing_pad[0] = new Pad(padx[0], pady[0], pad_width, pad_height);
	controller.landing_pad[1] = new Pad(padx[1], pady[1], pad_width, pad_height);
	//create the terrain
	terrain_create(controller, 0, 0, 300+(rand()%300), padx[0]-pad_width/2, pady[0]-pad_height/2);
	terrain_create(controller, 1, padx[0]+pad_width/2, pady[0]-pad_height/2, padx[1]-pad_width/2, pady[1]-pad_height/2);
	terrain_create(controller, 2, padx[1]+pad_width/2, pady[1]-pad_height/2, 800, 300+(rand()%300));
}

/*
 * Initialize X and create a window
 */
void initX(int argc, char *argv[], XInfo &xInfo, Controller &controller) {
	XSizeHints hints;
	unsigned long white, black;

   /*
	* Display opening uses the DISPLAY	environment variable.
	* It can go wrong if DISPLAY isn't set, or you don't have permission.
	*/	
	xInfo.display = XOpenDisplay("");
	if (!xInfo.display){
		error("Can't open display.");
	}
	
   /*
	* Find out some things about the display you're using.
	*/
	xInfo.screen = DefaultScreen(xInfo.display);

	white = XWhitePixel(xInfo.display, xInfo.screen);
	black = XBlackPixel(xInfo.display, xInfo.screen);
	
	Colormap colormap;
	colormap = DefaultColormap(xInfo.display, 0);
	XColor red_col;
	char red[] = "#FF0000";
	XColor yellow_col;
	char yellow[] = "#FFFF00";
	
	xInfo.width = 800;
	xInfo.height = 600;
	
	hints.x = 0;
	hints.y = 0;
	hints.width = xInfo.width;
	hints.height = xInfo.height;
	hints.flags = PSize;

	xInfo.window = XCreateSimpleWindow(
		xInfo.display,						// display where window appears
		DefaultRootWindow(xInfo.display),	// window's parent in window tree
		hints.x, hints.y,					// upper left corner location
		hints.width, hints.height,			// size of the window
		5,									// width of window's border
		black,								// window border colour
		white);								// window background colour
		
	XSetStandardProperties(
		xInfo.display,		// display containing the window
		xInfo.window,		// window whose properties are set
		"XLander",			// window's title
		"XL",				// icon's title
		None,				// pixmap for the icon
		argv, argc,			// applications command line args
		&hints );			// size hints for the window

	// create the graphics contexts
	xInfo.gcb = XCreateGC(xInfo.display, xInfo.window, 0, 0);	//black
	XSetForeground(xInfo.display, xInfo.gcb, black);
	XSetBackground(xInfo.display, xInfo.gcb, white);
	xInfo.gcw = XCreateGC(xInfo.display, xInfo.window, 0, 0);	//white
	XSetForeground(xInfo.display, xInfo.gcw, white);
	XSetBackground(xInfo.display, xInfo.gcw, black);
	xInfo.gcr = XCreateGC(xInfo.display, xInfo.window, 0, 0);	//red
	XParseColor(xInfo.display, colormap, red, &red_col);
	XAllocColor(xInfo.display, colormap, &red_col);
	XParseColor(xInfo.display, colormap, yellow, &yellow_col);
	XAllocColor(xInfo.display, colormap, &yellow_col);
	XSetForeground(xInfo.display, xInfo.gcr, red_col.pixel);
	XSetBackground(xInfo.display, xInfo.gcr, yellow_col.pixel);
	
	// create pixmap
	int depth = DefaultDepth(xInfo.display, DefaultScreen(xInfo.display));
	xInfo.pixmap = XCreatePixmap(xInfo.display, xInfo.window, xInfo.width, xInfo.height, depth);
	
	// set the input events from the window manager
    XSelectInput(xInfo.display, xInfo.window, KeyPressMask | StructureNotifyMask);
	
	//create the main menu and warning
	controller.warning = new Warning();
	controller.main = new Menu();
	
	//create objects
	initObjects(controller);

	/*
	 * Put the window on the screen.
	 */
	XMapRaised( xInfo.display, xInfo.window );
	
	XFlush(xInfo.display);
	usleep(1000);	// let server get set up before sending drawing commands
}

// get microseconds
unsigned long now() {
	timeval tv;
	gettimeofday(&tv, NULL);
	return tv.tv_sec * 1000000 + tv.tv_usec;
}
