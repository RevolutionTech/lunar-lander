/* - - - - - - - - - - - - - - - - - - - - - -
Name: Lucas Connors
Course: CS 349
Assignment: A1
File: ship.h
- - - - - - - - - - - - - - - - - - - - - - */

#ifndef __SHIP_H__
#define __SHIP_H__

#include "xlander.h"

class Ship : public Displayable {
	public:
		double vx, vy, g;
		double speed;
		bool collision;
		
		Ship(Controller &controller, double x, double y, int size);				//constructor
		
		int getX();										//getters
		int getY();
		int getSize();
		
		void decay();
		void move();
		void debug_switch();
		void collision_explode();
		void pause();
		void unpause();
		
		void paint(XInfo& xInfo);
	
	private:
		double x, y, d;
		int size;
		bool debug;
		int frame;
		bool paused;
		double vx_p, vy_p, speed_p, g_p;
		Controller *controller;
};

#endif /* __SHIP_H__ */
