/* - - - - - - - - - - - - - - - - - - - - - -
Name: Lucas Connors
Course: CS 349
Assignment: A1
File: paint.h
- - - - - - - - - - - - - - - - - - - - - - */

#ifndef __PAINT_H__
#define __PAINT_H__

#include "xlander.h"

class XInfo;

/*
 * An abstract class representing displayable things.
 */
class Displayable {
	public:
		bool in_menu;
		
	    virtual void paint(XInfo& xinfo) = 0;
};

class Controller;

class Ship;
class Pad;
class Terrain;
class Menu;

void drawEverything(XInfo &xInfo, Controller &controller);

#endif /* __PAINT_H__ */
