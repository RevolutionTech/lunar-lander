/* - - - - - - - - - - - - - - - - - - - - - -
Name: Lucas Connors
Course: CS 349
Assignment: A1
File: paint.cpp
- - - - - - - - - - - - - - - - - - - - - - */

#include "xlander.h"

void Warning::paint(XInfo& xInfo){
	Display *display = xInfo.display;
	Window win = xInfo.pixmap;
	GC black = xInfo.gcb;
	
	XDrawString(display, win, black, xInfo.width/2, xInfo.height/2, "too small", 9);
}

void Menu::paint(XInfo& xInfo){
	if (this->in_menu){
		Display *display = xInfo.display;
		Window win = xInfo.pixmap;
		GC black = xInfo.gcb;
		
		switch (this->menu_type){
			case MT_MAIN:
				XDrawString(display, win, black, 100, 50, "Lunar Lander", 12);
				XDrawString(display, win, black, 100, 100, "Press SPACE to start (and to pause in-game)", 43);
				XDrawString(display, win, black, 100, 150, "Press the arrow keys to activate the ship's thrusters", 53);
				break;
			case MT_GAMEOVER:
				XDrawString(display, win, black, 100, 50, "GAME OVER", 9);
				XDrawString(display, win, black, 100, 100, "Press SPACE to play again", 25);
				break;
			case MT_WIN:
				XDrawString(display, win, black, 100, 50, "YOU WIN!", 8);
				XDrawString(display, win, black, 100, 100, "Press SPACE to play again", 25);
				break;
		}
	}
}

void Ship::paint(XInfo& xInfo){
	if (!this->in_menu){
		Display *display = xInfo.display;
		Window win = xInfo.pixmap;
		GC black = xInfo.gcb;
		GC white = xInfo.gcw;
		if (!paused){
			GC red = xInfo.gcr;
			double x = this->x;
			double y = this->y;
			int size = this->size;		//40

			XDrawLine(display, win, black, x, y, x-(size/3), y+(size*3/4));											//left leg
			XDrawLine(display, win, black, x, y, x+(size/3), y+(size*3/4));											//right leg
			XDrawLine(display, win, black, x-(size/3)-(size/5), y+(size*3/4), x-(size/3)+(size/5), y+(size*3/4));	//left foot
			XDrawLine(display, win, black, x+(size/3)-(size/5), y+(size*3/4), x+(size/3)+(size/5), y+(size*3/4));	//right foot
			XFillArc(display, win, black, x-(size/2), y-(size/2), size, size, 0, 360*64);							//circle
			XFillRectangle(display, win, white, x-(size/6), y-(size/6), size/3, size/3);							//window
	
			if (debug){
				XFillRectangle(display, win, black, x-(size/2), y-(size/2), size+1, (size*5/4)+1);					//debug
												//(the +1 to size just helps with sorting out some rounding issues on size=40)
			}
	
			if (collision){
				XSetLineAttributes(display, red, 4, LineDoubleDash, CapRound, JoinMiter);
				XDrawLine(display, win, red, x-3-frame, y-19-frame, x+2+frame, y+3+frame);
				XDrawLine(display, win, red, x-18-frame, y-11-frame, x+21+frame, y+15+frame);
				XDrawLine(display, win, red, x+19+frame, y-14-frame, x-13-frame, y+16+frame);
			}
		}
		else {
			XDrawString(display, win, black, 383, 150, "PAUSED", 6);
		}
	}
}

void Pad::paint(XInfo& xInfo){
	if (!this->in_menu){
		Display *display = xInfo.display;
		Window win = xInfo.pixmap;
		GC black = xInfo.gcb;
		GC white = xInfo.gcw;
		int width = this->width;
		int height = this->height;
		int numDetail = 5;

		XDrawRectangle(display, win, black, x-(width/2), y-(height/2), width, height);

		//draw detail
		for (int i=0; i<numDetail; ++i){
			XDrawLine(display, win, black, x-(width/2)+i*(width/numDetail), y-(height/2),
				x-(width/2)+(i+1)*(width/numDetail), y+(height/2));
			XDrawLine(display, win, black, x+(width/2)-i*(width/numDetail), y-(height/2),
				x+(width/2)-(i+1)*(width/numDetail), y+(height/2));
		}
	}
}

void Terrain::paint(XInfo& xInfo){
	if (!this->in_menu){
		Display *display = xInfo.display;
		Window win = xInfo.pixmap;
		GC black = xInfo.gcb;
	
		XDrawLines(display, win, black, &pts[0], pts.size(), CoordModeOrigin);	//lines
	}
}

void drawEverything(XInfo &xInfo, Controller &controller){
	XClearWindow(xInfo.display, xInfo.window);
	
	XFillRectangle(xInfo.display, xInfo.pixmap, xInfo.gcw, 0, 0, xInfo.width, xInfo.height);
	XDrawRectangle(xInfo.display, xInfo.pixmap, xInfo.gcb, 0, 0, 800, 600);
	
	if (controller.warning->display_warning){
		controller.warning->paint(xInfo);
	}
	else {
		controller.main->paint(xInfo);
		controller.player->paint(xInfo);
		for (int i=0; i<2; ++i){
			controller.landing_pad[i]->paint(xInfo);
		}
		for (int i=0; i<3; ++i){
			controller.terrain[i]->paint(xInfo);
		}
	}
	
	// copy buffer to window
	XCopyArea(xInfo.display, xInfo.pixmap, xInfo.window, xInfo.gcb, 
		0, 0, xInfo.width, xInfo.height,  // region of pixmap to copy
		(xInfo.width/2)-400, (xInfo.height/2)-300); // position to put pixmap in center of window

	/* flush all pending requests to the X server. */
	XFlush(xInfo.display);
}
