/* - - - - - - - - - - - - - - - - - - - - - -
Name: Lucas Connors
Course: CS 349
Assignment: A1
File: window.h
- - - - - - - - - - - - - - - - - - - - - - */

#ifndef __WINDOW_H__
#define __WINDOW_H__

#include "xlander.h"

#define MT_MAIN 0
#define MT_GAMEOVER 1
#define MT_WIN 2

// display, window, screen struct, and graphics contexts
struct XInfo {
	Display	*display;
	int screen;
	Window window;
	GC gcb;
	GC gcw;
	GC gcr;
	Pixmap	pixmap;		// double buffer
	int width;
	int height;
};

class Warning : public Displayable {
	public:
		bool display_warning;
		
		Warning();
		
		void paint(XInfo& xInfo);
};

class Menu : public Displayable {
	public:
		int menu_type;
		
		Menu();
		
		void paint(XInfo& xInfo);
};

// game controller
class Controller {
	public:
		Ship *player;
		Pad *landing_pad[2];
		Terrain *terrain[3];
		Menu *main;
		Warning *warning;
		
		Controller();
		
		void space();
		void pause();
		void destroy_all();
		void win();
		
	private:
		bool in_menu, paused;
};

void error(string str);
void handleResize(XInfo &xInfo, XEvent &event, Controller &controller);
void initObjects(Controller &controller);
void initX(int argc, char *argv[], XInfo &xInfo, Controller &controller);
unsigned long now();
void eventloop(XInfo &xInfo, Controller &controller);

#endif /* __WINDOW_H__ */
