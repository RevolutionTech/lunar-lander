/* - - - - - - - - - - - - - - - - - - - - - -
Name: Lucas Connors
Course: CS 349
Assignment: A1
File: collision.cpp
- - - - - - - - - - - - - - - - - - - - - - */

#include "xlander.h"

// find the cross product of two points
int crossp(XPoint u, XPoint v){
	return u.x*v.y - u.y*v.x;
}

// find the difference in two points
XPoint diffp(XPoint u, XPoint v){
	XPoint x;
	x.x = u.x-v.x;
	x.y = u.y-v.y;
	return x;
}

// checks scalars in a collision detection to see if a collision has occurred
bool collision_check_scalars(double s, double t){
	if (s >= 0 and s <= 1 and t >= 0 and t <= 1){
		return true;
	}
	else {
		return false;
	}
}

// checks collision on two line segments uv and xy
bool collision_check_lines(XPoint u, XPoint v, XPoint x, XPoint y){
	XPoint w = diffp(v, u);
	XPoint z = diffp(y, x);
	
	double s, t;
	s = (double)crossp(diffp(x, u), z) / (double)crossp(w, z);
	t = (double)crossp(diffp(x, u), w) / (double)crossp(w, z);
	
	return collision_check_scalars(s, t);
}

// checks collisions with the spaceship and the terrain and landing pads
void collision_check_all(Controller &controller){
	int ship_x = controller.player->getX();
	int ship_y = controller.player->getY();
	int ship_size = controller.player->getSize();
	XPoint ship_nw, ship_sw, ship_ne, ship_se;
	
	ship_nw.x = ship_x-ship_size/2;
	ship_nw.y = ship_y-ship_size/2;
	ship_sw.x = ship_x-ship_size/2;
	ship_sw.y = ship_y+ship_size/2;
	ship_ne.x = ship_x+ship_size/2;
	ship_ne.y = ship_y-ship_size/2;
	ship_se.x = ship_x+ship_size/2;
	ship_se.y = ship_y+ship_size/2;
	
	//collision detection for ship and terrain
	for (int i=0; i<3; ++i){
		for (int j=0; j<controller.terrain[i]->pts.size()-1; ++j){
			XPoint pta, ptb;
			pta = controller.terrain[i]->pts.at(j);
			ptb = controller.terrain[i]->pts.at(j+1);
			bool check =
				collision_check_lines(ship_nw, ship_sw, pta, ptb)
				|| collision_check_lines(ship_sw, ship_se, pta, ptb)
				|| collision_check_lines(ship_ne, ship_se, pta, ptb);
			if (check && !controller.player->collision){
				controller.player->collision_explode();
			}
		}
	}
	
	//collision detection for ship and landing pads
	for (int i=0; i<2; ++i){
		Pad *pad = controller.landing_pad[i];
		XPoint pta, ptb;
		pta.x = pad->x-(pad->width/2);
		pta.y = pad->y-(pad->height);
		ptb.x = pad->x+(pad->width/2);
		ptb.y = pad->y-(pad->height);
		bool check =
			collision_check_lines(ship_nw, ship_sw, pta, ptb)
			|| collision_check_lines(ship_ne, ship_se, pta, ptb);
		if (check && !controller.player->collision){
			if (controller.player->vx > 2 || controller.player->vy+controller.player->g > 2){
				controller.player->collision_explode();
			}
			else {
				controller.win();
			}
		}		
	}
}
