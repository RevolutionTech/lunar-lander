/* - - - - - - - - - - - - - - - - - - - - - -
Name: Lucas Connors
Course: CS 349
Assignment: A1
File: collision.h
- - - - - - - - - - - - - - - - - - - - - - */

#ifndef __COLLISION_H__
#define __COLLISION_H__

#include "xlander.h"

// find the cross product of two points
int crossp(XPoint u, XPoint v);

// find the difference in two points
XPoint diffp(XPoint u, XPoint v);

// checks scalars in a collision detection to see if a collision has occurred
bool collision_check_scalars(double s, double t);

// checks collision on two line segments uv and xy
bool collision_check_lines(XPoint u, XPoint v, XPoint x, XPoint y);

// checks collisions with the spaceship and the terrain and landing pads
void collision_check_all(Controller &controller);

#endif /* __COLLISION_H__ */
