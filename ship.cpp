/* - - - - - - - - - - - - - - - - - - - - - -
Name: Lucas Connors
Course: CS 349
Assignment: A1
File: ship.cpp
- - - - - - - - - - - - - - - - - - - - - - */

#include "xlander.h"

//constructor
Ship::Ship(Controller &controller, double x, double y, int size){
	this->controller = &controller;
	this->x = x;
	this->y = y;
	this->size = size;
	this->vx = 0;
	this->vy = 0;
	this->speed = 0.25;
	this->g = 4;
	this->d = 1.02;
	this->debug = false;
	this->collision = false;
	this->paused = false;
	this->in_menu = true;
}

//getters
int Ship::getX(){
	return x;
}
int Ship::getY(){
	return y;
}
int Ship::getSize(){
	return size;
}

void Ship::decay(){
	double d = this->d;
	if (this->vx > -0.1 && this->vx < 0.1){
		this->vx = 0;
	}
	else {
		this->vx = this->vx/d;
	}
	if (this->vy > -0.1 && this->vy < 0.1){
		this->vy = 0;
	}
	else {
		this->vy = this->vy/d;
	}
}

void Ship::move(){
	this->x += this->vx;
	this->y += this->vy + this->g;
	this->decay();
	
	if (this->x+this->size/2 < 0){			//wrap around screen
		this->x = 800+this->size/2-1;
	}
	else if (this->x-this->size/2 > 800){
		this->x = -this->size/2+1;
	}
	
	++this->frame;
	if (this->frame>9){
		if (this->collision){
			controller->destroy_all();
		}
		else {
			this->frame = 0;
		}
	}
}

void Ship::debug_switch(){
	this->debug = !this->debug;
}

void Ship::collision_explode(){
	this->speed = 0;
	this->frame = 0;
	this->collision = true;
}

void Ship::pause(){
	this->paused = true;
	this->speed_p = this->speed;
	this->speed = 0;
	this->vx_p = this->vx;
	this->vx = 0;
	this->vy_p = this->vy;
	this->vy = 0;
	this->g_p = this->g;
	this->g = 0;
}

void Ship::unpause(){
	this->speed = this->speed_p;
	this->vx = this->vx_p;
	this->vy = this->vy_p;
	this->g = this->g_p;
	this->paused = false;
}
