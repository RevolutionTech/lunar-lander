/* - - - - - - - - - - - - - - - - - - - - - -
Name: Lucas Connors
Course: CS 349
Assignment: A1
File: ground.h
- - - - - - - - - - - - - - - - - - - - - - */

#ifndef __GROUND_H__
#define __GROUND_H__

#include "xlander.h"

class Pad : public Displayable {
	public:
		int x;
		int y;
		int width;
		int height;
	
		Pad(int x, int y, int width, int height);		//constructor
		
		void paint(XInfo& xInfo);
};

class Terrain : public Displayable {
	public:
		vector<XPoint> pts;
		
		Terrain();										//constructor
		
		// add another point to the line
		void add_point(int x, int y);
		
		void paint(XInfo& xInfo);
};

// create terrain
void terrain_create(Controller &controller, int i, int x, int y, int xn, int yn);

#endif /* __GROUND_H__ */
