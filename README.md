# Lunar Lander (XLander)
# Created by: Lucas Connors
# for CS 349 (User Interfaces)

![Lunar Lander](http://revolutiontech.ca/media/img/lunar-lander.jpg)

***

## About

XLander is a re-creation of the classic Lunar Lander game created in X windows (ouch! I know!). The project was created for an assignment in CS 349 of Spring 2013.

Each time a new game starts, the terrain is randomly auto-generated.

## Prerequisites

Lunar Lander requires g++ and X11, which you can install on Debian with:

`sudo apt-get install g++`

`sudo apt-get install libx11-dev`

## Running

Then Lunar Lander can be run with the following command:

`make run`

After a brief moment, a window should appear.